﻿using System;
using ReactiveUI;
using Ninject;

namespace Test.iOS
{
	public class BaseViewController<T> : ReactiveViewController, IViewFor<T> where T: ReactiveObject
	{
		T _viewModel;
		public T ViewModel {
			get {
				return _viewModel;
			}
			set {
				this.RaiseAndSetIfChanged(ref _viewModel, value);
			}
		}
			
		object IViewFor.ViewModel
		{
			get { return ViewModel; }
			set { ViewModel = (T)value; }
		}

		protected IKernel Kernel {
			get{ 
				return KernelLoader.Instance.Kernel;
			}
		}

		protected virtual T BuildViewModel(){
			return Kernel.Get<T> ();
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad ();
			_viewModel = BuildViewModel ();
		}

		public BaseViewController (IntPtr handle) : base (handle)
		{
		}

		/*
		public BaseViewController ()
		{
		}*/
	}
}

