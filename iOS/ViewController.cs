﻿using System;
using ReactiveUI;
		
using UIKit;
using System.Diagnostics;

namespace Test.iOS
{
	public partial class ViewController : BaseViewController<TestViewModel>
	{
		int count = 1;

		public ViewController (IntPtr handle) : base (handle)
		{		
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			/*ViewModel = new TestViewModel ();

			Console.WriteLine (ViewModel.TestValue);*/

			this.OneWayBind(ViewModel, x => x.TestValue, x => x.TestLabel.Text);

			// Perform any additional setup after loading the view, typically from a nib.
			Button.AccessibilityIdentifier = "myButton";
			Button.TouchUpInside += delegate {
				ViewModel.TestCommand.Execute("It worked");
			};

			FetchIPButton.TouchUpInside += delegate {
				var result = ViewModel.FetchIPCommand.ExecuteAsync();
				result.Subscribe(x => Debug.WriteLine("Request:", x));
			};
		}

		public override void DidReceiveMemoryWarning ()
		{		
			base.DidReceiveMemoryWarning ();		
			// Release any cached data, images, etc that aren't in use.		
		}
	}
}
