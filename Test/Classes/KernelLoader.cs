﻿using System;
using Ninject;

namespace Test
{
	/** This is our dependency injection HQ
	 * By making a singleton to handle the Ninject kernel we can keep tabs of dependencies for everyone in one convenient spot 
	**/

	public class KernelLoader
	{
		// Singletone shit
		private static KernelLoader instance;

		// The Ninject kernel, very important
		public IKernel Kernel { get; private set; }

		public static KernelLoader Instance {
			get {
				if (instance == null) {
					instance = new KernelLoader ();

					// Create the Ninject Kernel
					instance.Kernel = new StandardKernel();

					// Bind dependencies
					instance.Kernel.Bind<IWeapon>().To<Sword>();
				}
				return instance;
			}
		}
	}
}

