﻿using System;
using System.Diagnostics;

namespace Test
{
	public class Shuriken : IWeapon
	{
		public void Hit(string target)
		{
			Debug.WriteLine("Pierced {0}'s armor", target);
		}
	}
}

