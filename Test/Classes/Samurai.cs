﻿using System;
using Ninject;

namespace Test
{
	/** A test class for Ninject
	 * This is where the magic happens (half of it anyway
	**/
	public class Samurai
	{
		/** The inject keyword makes it so that Ninject takes care of our dependency for us
		 * all the need to do is make sure we use Kernel.Get instead of new Samurai() to create
		 * our instances
		**/
		[Inject]
		public IWeapon weapon { get; set; }

		// This method will call the weapon's method, we use it to test proper DI has taken place
		public void Attack(string target)
		{
			this.weapon.Hit(target);
		}
	}
}

