﻿using System;
using System.Diagnostics;

namespace Test
{
	/** This is a class that implements the IWeapon interface, we need to bind this
	 * to the interface to make sure it gets used
	 **/
	public class Sword : IWeapon
	{
		public void Hit(string target)
		{
			Debug.WriteLine("Chopped {0} clean in half", target);
		}
	}
}

