﻿using System;
using Ninject.Modules;

namespace Test
{
	public class NinjaModule : NinjectModule
	{
		public override void Load ()
		{
			Bind<IWeapon>().To<Sword>();
		}
	}
}

