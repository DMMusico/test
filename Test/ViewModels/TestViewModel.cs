﻿using System;
using ReactiveUI;
using Ninject;
using System.IO;
using System.Diagnostics;
using System.Net.Http;
using System.Collections.Generic;
using System.Reactive.Linq;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Text.RegularExpressions;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace Test
{

	public class FlickrPhotoViewModel : ReactiveObject
	{
		private string title;
		private string detail;
		private string url;

		public string Title
		{
			get { return title; }
			set { this.RaiseAndSetIfChanged(ref title, value); }
		}
		public string Detail
		{
			get { return detail; }
			set { this.RaiseAndSetIfChanged(ref detail, value); }
		}
		public string Url
		{
			get { return url; }
			set { this.RaiseAndSetIfChanged(ref url, value); }
		}
	}

	public class TestViewModel : ReactiveObject
	{
		string _TestValue;
		public string TestValue {
			get { return _TestValue; }
			set { this.RaiseAndSetIfChanged(ref _TestValue, value); }
		}

		string _SecondTestValue;
		public string SecondTestValue {
			get { return _SecondTestValue; }
			set { this.RaiseAndSetIfChanged(ref _SecondTestValue, value); }
		}

		public ReactiveCommand<object> TestCommand { get; private set;}
		public ReactiveCommand<string> FetchIPCommand { get; private set;}

		[Inject]
		public Samurai Yasuo { get; set; }

		string _SearchTerm;
		public string SearchTerm
		{
			get { return _SearchTerm; }
			set { this.RaiseAndSetIfChanged(ref _SearchTerm, value); }
		}

		public ReactiveCommand<ReactiveList<FlickrPhotoViewModel>> ExecuteSearch { get; protected set; }

		ObservableAsPropertyHelper<List<FlickrPhoto>> _SearchResults;
		public List<FlickrPhoto> SearchResults => _SearchResults.Value;

		ObservableAsPropertyHelper<bool> _SpinnerVisibility;
		public bool SpinnerVisibility => _SpinnerVisibility.Value;

		private readonly ReactiveList<FlickrPhotoViewModel> photos = new ReactiveList<FlickrPhotoViewModel>();
		public IReadOnlyReactiveList<FlickrPhotoViewModel> Photos
		{
			get { return photos; }
		}

		ObservableAsPropertyHelper<string> _testString;
		public string testString => _testString.Value;

		public ReactiveList<string> TestList;

		public TestViewModel ()
		{
			TestList = new ReactiveList<string>();
			TestList.ItemsAdded.Subscribe(_ => {
				Debug.WriteLine("Something was added!");
			});
			TestList.ItemsRemoved.Subscribe(_ => {
				Debug.WriteLine("Something was removed!");
			});

			TestList.Add("Shit");
			TestList.Add("More bullshit");
			TestList.Remove("More bullshit");
			TestList.Add("Worthless junk");

			this.TestValue = "Test!";
			TestCommand = ReactiveCommand.Create();
			TestCommand.Subscribe(x => {
				this.TestValue = x.ToString();
				this.SecondTestValue = "Test";
			});

			FetchIPCommand = ReactiveCommand.CreateAsyncTask (async _ => {
				HttpClient httpClient = new HttpClient ();
				string s = await httpClient.GetStringAsync ("http://ip.jsontest.com");
				return s;
			});

			_testString = this.WhenAnyValue(x => x.TestValue).Select(x => "Not " + x).ToProperty(this, x => x.testString, _TestValue);
			this.WhenAnyValue(x => x.testString).Where(x => x.Contains("works")).InvokeCommand(FetchIPCommand);

			// Here is the Dependency injection
			Yasuo = KernelLoader.Instance.Kernel.Get<Samurai> (); // No new Samurai(), we need Ninject to take care of this
			Yasuo.Attack ("Teemo"); // HASAGI!!

			ExecuteSearch = ReactiveCommand.CreateAsyncTask(parameter => GetSearchResultsFromFlickr(this.SearchTerm));

			this.WhenAnyValue(x => x.SearchTerm)
				.Throttle(TimeSpan.FromMilliseconds(800), RxApp.MainThreadScheduler)
				.Select(x => x?.Trim())
				.DistinctUntilChanged()
				.Where(x => !String.IsNullOrWhiteSpace(x))
				.InvokeCommand(ExecuteSearch);

			_SpinnerVisibility = ExecuteSearch.IsExecuting
				.Select(x => x ? true : false)
				.ToProperty(this, x => x.SpinnerVisibility, false);

			ExecuteSearch.ThrownExceptions.Subscribe(ex => {/* Handle errors here */});
			ExecuteSearch.Subscribe(x => {
				using (photos.SuppressChangeNotifications())
				{
					photos.Clear();
					foreach (FlickrPhotoViewModel item in x) {
						photos.Add(item);
					}
				}
			});
				
			//_SearchResults = ExecuteSearch.ToProperty(this, x => x.SearchResults, new List<FlickrPhoto>());
			//ExecuteSearch.ToProperty(this, x => x.Photos, new ReactiveList<FlickrPhotoViewModel>());

			photos.Add(new FlickrPhotoViewModel(){Title = "Test 1", Detail = "You suck", Url = "http://www.reactiveui.sucks/fuckyou.jpg"});
			photos.Add(new FlickrPhotoViewModel(){Title = "Test 2", Detail = "You suck", Url = "http://www.reactiveui.sucks/fuckyou.jpg"});
			photos.Add(new FlickrPhotoViewModel(){Title = "Test 3", Detail = "You suck", Url = "http://www.reactiveui.sucks/fuckyou.jpg"});

		}

		public static async Task<ReactiveList<FlickrPhotoViewModel>> GetSearchResultsFromFlickr(string searchTerm)
		{
			HttpClient httpClient = new HttpClient ();
			string jsonString = await httpClient.GetStringAsync ("https://dl.dropboxusercontent.com/u/684065/Xamarin/test.json");
			List<JObject> stuff = JsonConvert.DeserializeObject<List<JObject>>(jsonString);

			var items = new List<FlickrPhoto> ();
			var photos = new ReactiveList<FlickrPhotoViewModel>();
			foreach (JObject item in stuff) {
				photos.Add(new FlickrPhotoViewModel(){Title = item.GetValue("Title").ToString(), Detail = "You suck", Url = "http://www.reactiveui.sucks/fuckyou.jpg"});
				items.Add(new FlickrPhoto{Title = item.GetValue("Title").ToString(), Description = "You suck", Url = "http://www.reactiveui.sucks/fuckyou.jpg"});
			}

			/*var items = new List<FlickrPhoto>{
				new FlickrPhoto{Title = "Fuck you", Description = "You suck", Url = "http://www.reactiveui.sucks/fuckyou.jpg"},
				new FlickrPhoto{Title = "You piece of shit", Description = "You suck", Url = "http://www.reactiveui.sucks/fuckyou.jpg"},
				new FlickrPhoto{Title = "I will destroy you you", Description = "You suck", Url = "http://www.reactiveui.sucks/fuckyou.jpg"}};*/
			var ret = new List<FlickrPhoto> ();
			foreach (FlickrPhoto photo in items) {
				if (photo.Title.ToLower().Contains(searchTerm.ToLower()))
				{
					ret.Add(photo);
				}
			}
			return photos;
		}

		public class FlickrPhoto
		{
			public string Title { get; set; }
			public string Description { get; set; }
			public string Url { get; set; }
		}
	}
}
	
