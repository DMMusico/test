﻿using System;

namespace Test
{
	/** This is a test interface
	 * Because we are using Depency Injection we can make sure that every time we create an
	 * instance that requires an object that implements this interface we will have one
	 * automatically, courtesy of Ninject **/
	public interface IWeapon
	{
		void Hit(string target);
	}
}

