﻿using System;
using ReactiveUI;
using Ninject;

namespace Test.Droid
{
	public class BaseActivity<T>: ReactiveActivity, IViewFor<T> where T: ReactiveObject
	{
		protected T _viewModel;
		public T ViewModel
		{
			get { return _viewModel; }
			set { this.RaiseAndSetIfChanged(ref _viewModel, value); }
		}

		object IViewFor.ViewModel
		{
			get { return ViewModel; }
			set { ViewModel = (T)value; }
		}
			
		protected IKernel Kernel {
			get{ 
				return KernelLoader.Instance.Kernel;
			}
		}

		protected virtual T BuildViewModel(){
			return Kernel.Get<T> ();
		}

		protected override void OnCreate (Android.OS.Bundle savedInstanceState)
		{
			_viewModel = BuildViewModel ();
			base.OnCreate (savedInstanceState);
		}
	}
}

