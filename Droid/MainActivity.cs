﻿using Android.App;
using Android.Widget;
using Android.OS;
using ReactiveUI;
using System;
using Android.Views;
using System.Collections.Generic;
using Android.Content;

namespace Test.Droid
{
	public class PhotoItemView : ReactiveViewHost<FlickrPhotoViewModel>
	{
		public PhotoItemView (FlickrPhotoViewModel viewModel, Context ctx, ViewGroup parent) : base (ctx, Resource.Layout.PhotoItemView, parent)
		{
			ViewModel = viewModel;
			this.OneWayBind (ViewModel, vm => vm.Title, v => v.title.Text);
		}

		public TextView title { get; private set; }
	}

	[Activity (Label = "Test", MainLauncher = true, Theme = "@style/MyCustomTheme", Icon = "@mipmap/icon")]
	public class MainActivity : BaseActivity<TestViewModel>
	{
		//int count = 1;

		//		TestViewModel _ViewModel;
		//		public TestViewModel ViewModel
		//		{
		//			get { return _ViewModel; }
		//			set { this.RaiseAndSetIfChanged(ref _ViewModel, value); }
		//		}

		//		object IViewFor.ViewModel
		//		{
		//			get { return ViewModel; }
		//			set { ViewModel = (TestViewModel)value; }
		//		}

		public TextView TestTextView { get; private set; }

		public EditText SearchField { get; private set; }

		public ListView ResultsListView { get; private set; }

		public TextView PropertyTest { get; private set; }

		public class ResultsListAdapter : BaseAdapter<Test.TestViewModel.FlickrPhoto>
		{
			List<Test.TestViewModel.FlickrPhoto> items;
			Activity context;

			public ResultsListAdapter (Activity context, List<Test.TestViewModel.FlickrPhoto> items) : base ()
			{
				this.context = context;
				this.items = items;
			}

			public override long GetItemId (int position)
			{
				return position;
			}

			public override Test.TestViewModel.FlickrPhoto this [int position] {  
				get { return items [position]; }
			}

			public override int Count {
				get { return items.Count; }
			}

			public override View GetView (int position, View convertView, ViewGroup parent)
			{
				View view = convertView; // re-use an existing view, if one is available
				if (view == null) // otherwise create a new one
					view = context.LayoutInflater.Inflate (Android.Resource.Layout.SimpleListItem1, null);
				view.FindViewById<TextView> (Android.Resource.Id.Text1).Text = items [position].Title;
				return view;
			}
		}

		protected override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);

			// Set our view from the "main" layout resource
			SetContentView (Resource.Layout.Main);
			/*
//			_ViewModel = KernelLoader.Instance.Kernel.Get<TestViewModel> ();
//
//
//			ViewModel = new TestViewModel();*/

			this.WireUpControls ();

			/*var items = new string[] { "Vegetables","Fruits","Flower Buds","Legumes","Bulbs","Tubers" };
			var ListAdapter = new ResultsListAdapter(this, ViewModel.SearchResults);
			ResultsListView.Adapter = ListAdapter;*/

			/*ViewModel.WhenAnyValue(x => x.SearchResults).Subscribe(x => {
				var ListAdapter = new ResultsListAdapter(this, ViewModel.SearchResults);
				ResultsListView.Adapter = ListAdapter;
			});*/
			var adapter = new ReactiveListAdapter<FlickrPhotoViewModel> (
				              ViewModel.Photos,
				              (viewModel, parent) => new PhotoItemView (viewModel, this, parent));
			ResultsListView.Adapter = adapter;

			this.OneWayBind (this.ViewModel, x => x.TestValue, x => x.TestTextView.Text);
			this.OneWayBind (this.ViewModel, x => x.testString, x => x.PropertyTest.Text);

			this.Bind (this.ViewModel, x => x.SearchTerm, x => x.SearchField.Text);

			ViewModel.WhenAnyValue (x => x.SecondTestValue).Subscribe (x => System.Diagnostics.Debug.WriteLine ("Value changed!"));

			// Get our button from the layout resource,
			// and attach an event to it
			Button button = FindViewById<Button> (Resource.Id.myButton);
			
			button.Click += delegate {
				ViewModel.TestCommand.Execute ("It works");
			};

			Button ipButton = FindViewById<Button> (Resource.Id.ipButton);
	
			ipButton.Click += delegate {
				
				var result = ViewModel.FetchIPCommand.ExecuteAsync ();
				result.Subscribe (x => System.Diagnostics.Debug.WriteLine ("Result:", x));
			};
		}
	}
}


