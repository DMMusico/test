#pragma warning disable 1591
// ------------------------------------------------------------------------------
//  <autogenerated>
//      This code was generated by a tool.
//      Mono Runtime Version: 4.0.30319.17020
// 
//      Changes to this file may cause incorrect behavior and will be lost if 
//      the code is regenerated.
//  </autogenerated>
// ------------------------------------------------------------------------------

[assembly: Android.Runtime.ResourceDesignerAttribute("Test.Droid.Resource", IsApplication=true)]

namespace Test.Droid
{
	
	
	[System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Android.Build.Tasks", "1.0.0.0")]
	public partial class Resource
	{
		
		static Resource()
		{
			global::Android.Runtime.ResourceIdManager.UpdateIdValues();
		}
		
		public static void UpdateIdValues()
		{
			global::Splat.Resource.String.library_name = global::Test.Droid.Resource.String.library_name;
		}
		
		public partial class Attribute
		{
			
			static Attribute()
			{
				global::Android.Runtime.ResourceIdManager.UpdateIdValues();
			}
			
			private Attribute()
			{
			}
		}
		
		public partial class Id
		{
			
			// aapt resource value: 0x7f060003
			public const int PropertyTest = 2131099651;
			
			// aapt resource value: 0x7f060005
			public const int ResultsListView = 2131099653;
			
			// aapt resource value: 0x7f060004
			public const int SearchField = 2131099652;
			
			// aapt resource value: 0x7f060000
			public const int TestTextView = 2131099648;
			
			// aapt resource value: 0x7f060002
			public const int ipButton = 2131099650;
			
			// aapt resource value: 0x7f060001
			public const int myButton = 2131099649;
			
			// aapt resource value: 0x7f060006
			public const int title = 2131099654;
			
			static Id()
			{
				global::Android.Runtime.ResourceIdManager.UpdateIdValues();
			}
			
			private Id()
			{
			}
		}
		
		public partial class Layout
		{
			
			// aapt resource value: 0x7f030000
			public const int Main = 2130903040;
			
			// aapt resource value: 0x7f030001
			public const int PhotoItemView = 2130903041;
			
			static Layout()
			{
				global::Android.Runtime.ResourceIdManager.UpdateIdValues();
			}
			
			private Layout()
			{
			}
		}
		
		public partial class Mipmap
		{
			
			// aapt resource value: 0x7f020000
			public const int Icon = 2130837504;
			
			static Mipmap()
			{
				global::Android.Runtime.ResourceIdManager.UpdateIdValues();
			}
			
			private Mipmap()
			{
			}
		}
		
		public partial class String
		{
			
			// aapt resource value: 0x7f040002
			public const int app_name = 2130968578;
			
			// aapt resource value: 0x7f040001
			public const int hello = 2130968577;
			
			// aapt resource value: 0x7f040000
			public const int library_name = 2130968576;
			
			static String()
			{
				global::Android.Runtime.ResourceIdManager.UpdateIdValues();
			}
			
			private String()
			{
			}
		}
		
		public partial class Style
		{
			
			// aapt resource value: 0x7f050000
			public const int MyCustomTheme = 2131034112;
			
			static Style()
			{
				global::Android.Runtime.ResourceIdManager.UpdateIdValues();
			}
			
			private Style()
			{
			}
		}
	}
}
#pragma warning restore 1591
