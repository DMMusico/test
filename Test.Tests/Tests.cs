﻿
using System;
using NUnit.Framework;

namespace Test.Tests
{
	[TestFixture]
	public class Tests
	{
		[Test]
		public void SumTest ()
		{
			int test = 2 + 3;
			Assert.True (test == 5);
		}

		[Test]
		public void StringLengthCheck ()
		{
			string test = "Too long";
			Assert.True (test.Length < 6);
		}

//		[Test]
//		[Ignore ("another time")]
//		public void Ignore ()
//		{
//			Assert.True (false);
//		}
	}
}
